# Variable and method name
## Use intention-revealing name

{- Not Preferred -}
```swift
private let d: Int //elapsed time in days
```

{+ Preferred +}
```swift
private let elapsedTimeInDays: Int
private let daysSinceCreation: Int
private let daysSinceModification: Int
private let numberOfPresentParticipants: Int
```

## Use pronounceable name

{- Not Preferred -}
```swift
private let genymdhms: Int64
private let modymdhms: Int64
```

{+ Preferred +}
```swift
private let generationTimeStamp: Int64
private let modificationTimeStamp: Int64
```

## Use namespaces instead of prefixing names

Most modern programming languages (including PHP and Python) support namespaces.

{- Not Preferred -}
```swift
Class Participant {
    private let m_name: String
    private let m_age: Int
}
```

{+ Preferred +}
```swift
Class Participant {
    private let name: String
    private let age: Int
}
```

## Don't be cute

{- Not Preferred -}
```swift
monster.whack()
```

{+ Preferred +}
```swift
monster.kill()
```

## Use one word per concept

Be consistent. For example, don’t use get and fetch to do the same thing in different classes

{- Not Preferred -}
```swift
fetchMonsters()
getPlayers()
loadItems()
```

{+ Preferred +}
```swift
getMonsters()
getPlayers()
getItems()
```

## Use solution domain names

People reading your code will be other programmers so they understand solution domain terminology, so make the most of it. - For example, (`jobQueue`) is better than (`jobs`)

## Use verbs for functions names and nouns for classes and attributes

{- Not Preferred -}
```swift
class GetParticipant {
    private let getName: String
    private let birthday: Date

    func birthday() -> Date {
        return self.birthday
    }
}
```

{+ Preferred +}
```swift
class Participant {
    private let name: String
    private var age: Int = 0

    func increaseAgeByOne() {
        self.age += 1
    }
}
```

## General guidelines
- using camel case (not snake case)
- using uppercase for types (and protocols), lowercase for everything else
- including all needed words while omitting needless words
- beginning factory methods with make
- naming methods for their side effects
    - verb methods follow the -ed, -ing rule for the non-mutating version
    - boolean types should read like assertions
    - protocols that describe what something is should read as nouns
    - protocols that describe a capability should end in -able or -ible
- generally avoiding abbreviations
- choosing good parameter names that serve as documentation
- labeling closure and tuple parameters

# Better Fuctions
## The smaller the better
## A function should only do one thing

{- Not Preferred -}
```swift
override func viewDidLoad() {
    super.viewDidLoad()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = CGFloat(100.0)
    tableView.rowHeight = UITableViewAutomaticDimension
    mapView.isHidden = true
    searchController.searchResultsUpdater = self
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Search Customers"
    searchController.searchBar.delegate = self

    if #available(iOS 11, *) {
        navigationItem.searchController = searchController
        alternateSearchBarHeightConstraint.constant = 0
    } else {
        alternateSearchBar.delegate = self
        alternateSearchBar.showsCancelButton = true
    }

    definesPresentationContext = true
    activityIndicator.isHidden = true
    activityIndicator.stopAnimating()

    let dictionary = Bundle.main.infoDictionary!
    let version = dictionary["CFBundleShortVersionString"] as! String
    let build = dictionary["CFBundleVersion"] as! String
    let versionString = "Version \(version), Build \(build)"
    versionLabel.text = versionString
}
```

{+ Preferred +}
```swift
override func viewDidLoad() {
    configureTableView()
    configureSearchController()
    hideActivityIndicator()
    hideMapView()
    setVersionInfo()
}

private func hideActivityIndicator() {
    activityIndicator.isHidden = true
    activityIndicator.stopAnimating()
}

private func hideMapView() {
    mapView.isHidden = true
}

private func setVersionInfo() {
    let dictionary = Bundle.main.infoDictionary!
    let version = dictionary["CFBundleShortVersionString"] as! String
    let build = dictionary["CFBundleVersion"] as! String
    let versionString = "Version \(version), Build \(build)"
    versionLabel.text = versionString
}

private func configureTableView() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = CGFloat(100.0)
    tableView.rowHeight = UITableViewAutomaticDimension
}

private func configureSearchController() {
    searchController.searchResultsUpdater = self
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Search Customers"
    searchController.searchBar.delegate = self

    if #available(iOS 11, *) {
        navigationItem.searchController = searchController
        alternateSearchBarHeightConstraint.constant = 0
    } else {
        alternateSearchBar.delegate = self
        alternateSearchBar.showsCancelButton = true
    }

    definesPresentationContext = true
}
```

## No nested control structure

{- Not Preferred -}
```swift
func calculatePay(employee: Employee) -> Pay
    switch employee.type {
    case .commissioned:
        return calculateCommissionedPay(employee: employee)
    case .hourly:
        return calculateHourlyPay(employee: employee)
    case .salaried:
        return calculateSalariedPay(employee: employee)
}

func deliverPay(employee: Employee, money: Pay)
    switch employee.type {
    case .commissioned:
        deliverCommissionedPay(employee: employee, money: money)
    case .hourly:
        deliverHourlyPay(employee: employee, money: money)
    case .salaried:
        deliverSalariedPay(employee: employee, money: money)
}
```

{+ Preferred +}
```swift
protocol Employee: class {
    func calculatePay() -> Pay
    func deliverPay()
}

class CommissionedEmployee: Employee {
    func calculatePay() -> Pay {
        //own implementation
    }

    func deliverPay() {
        //own implementation
    }
}

class HourlyEmployee: Employee {
    func calculatePay() -> Pay {
        //own implementation
    }

    func deliverPay() {
        //own implementation
    }
}

class SalariedEmployee: Employee {
    func calculatePay() -> Pay {
        //own implementation
    }

    func deliverPay() {
        //own implementation
    }
}

class EmployeeFactory {
    func makeEmployee(employeeRecord: EmployeeRecord) -> Employee {
        switch employeeRecord.type {
        case .commissioned:
            return CommissionedEmployee(employeeRecord: employeeRecord)
        case .hourly:
            return HourlyEmployee(employeeRecord: employeeRecord)
        case .salaried:
            return SalariedEmployee(employeeRecord: employeeRecord)
    }
}
```

## Less arguments are better

More than three arguments are evil. For example:

{- Not Preferred -}
```swift
func makeCircle(x: double, y: double, radius: double)
```

{+ Preferred +}
```swift
func makeCircle(center: Point, radius: double)
```

## No side effects

Functions must only do what the name suggests and nothing else.

{- Not Preferred -}
```swift
func configureTableView() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = CGFloat(100.0)
    tableView.rowHeight = UITableViewAutomaticDimension

    searchController.searchResultsUpdater = self
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Search Customers"
    searchController.searchBar.delegate = self
}
```

{+ Preferred +}
```swift
func configureTableView() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = CGFloat(100.0)
    tableView.rowHeight = UITableViewAutomaticDimension
}

func configureSearchController() {
    searchController.searchResultsUpdater = self
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Search Customers"
    searchController.searchBar.delegate = self
}
```

## Prefer exceptions to returning error codes

Throwing exceptions is better than returning different codes dependent on errors.

Asking for forgiveness is easier than requesting permission. Use try/catch instead of conditions if possible.

{- Not Preferred -}
```swift
if deletePage(page: page) == E_OK {
    if registry.deleteReference(pageName: page.name) == E_OK {
        if configKeys.deleteKey(key: page.name.makeKey()) == E_OK {
            logger.log("page deleted")
        } else {
            logger.log("config key not deleted")
        }
    } else {
        logger.log("deleteReference from registry failed")
    }
} else {
    logger.log("delete failed")
    return E_ERROR
}

```

{+ Preferred +}
```swift
do {
    try deletePage(page: page)
    try registry.deleteReference(pageName: page.name)
    try configKeys.deleteKey(key: page.name.makeKey())
} catch {
    logger.log("error: \(error.localizedDescription)")
}
```

## Don't repeat yourself

Functions must be atomic, use Utility or Helper functions.

# Comments
## Don't comment bad code, rewrite it
## If code is readable, you don't need comments

{- Not Preferred -}
```swift
// Check to see of the employee is eligible for full benefits
if employee.age > 65 && employee.yearsOfService > 10 && employee.hasNoBadRecords {
    employee.applyFullBenefits()
}
```

{+ Preferred +}
```swift
if employee.isEligibleForFullBenefits() {
    employee.applyFullBenefits()
}
```

## Explain your intention in comments

Sometimes a comment goes beyond just useful information about the implementation and provides the intent behind a decision.

```swift
//This is our best attempt to get a race condition //by creating large number of threads.
for i in 0...25000 {
    let widgetBuilderThread = WidgetBuilderThread()
    let thread = Thread(widgetBuilderThread)
    thread.start()
}
```

## Warn of consequences in comments

```swift
//Don't run unless you have some time to kill
func testWithReallyBigFile() {
    writeLinesToFile(10000000)
    response.setBody(testFile)
    response.readtToSend(self)
    let responseString = output.toString()
    assertTrue(bytesSent > 10000000)
}
```

## Emphasis important points in comments

```swift
// the trim function is very important, in most cases the username has a trailing space
```

## Any comments other than the above should be avoided

## Noise comments are bad

```swift
// The day of the month.
private var dayOfThemMonth: String
```

## Never leave code commented

Perhaps this is the most important point. With modern source control software such as Git you can always investigate and revert back to historical code. It is ok to comment code when debugging or programming, but you should never ever check in commented code.

# Classes
## Classes should be small! Avoid "God class"

The name of a class should describe what responsibilities it fulfills. In fact, naming is probably the first way of helping determine class size. If we cannot derive a concise name for a class, then it’s likely too large.

{- Not Preferred -}
```swift
class SuperDashboard {
func getCustomerLanguagePath() -> String
func setSystemConfigPath(systemConfigPath: String)
func getSystemConfigDocument() -> String
func setSystemConfigDocument(systemConfigDocument: Document)
func getGuruState() -> Bool
func getNoviceState() -> Bool
func getOpenSourceState() -> Bool
func showObject(object: MetaObject)
func showProgress(string: String)
func isMetadataDirty() -> Bool
func setIsMetadataDirty(isMetadataDirty: Bool)
func getLastFocusedComponent() -> Component
func setLastFocused(lastFocused: Component)
func setMouseSelectState(isMouseSelected: Bool)
func isMouseSelected() -> Bool
func getLanguageManager() -> LanguageManager
func getProject() -> Project
func getFirstProject() -> Project
}
```

{+ Preferred +}
```swift
// A single responsibility class
class Version {
func getMajorVersionNumber() -> Int
func getMinorVersionNumber() -> Int
func getBuildNumber() -> Int
}
```

## Cohesion

Classes should have a small number of instance variables. Each of the methods of a class should manipulate one or more of those variables.

In general the more variables a method manipulates the more cohesive that method is to its class. A class in which each variable is used by each method is maximally cohesive.

Example:

```swift
class Stack {
    private var topOfStack = 0
    private var elements = [Int]()

    func getSize() -> Int {
        return topOfStack
    }

    func push(element: Int) {
        topOfStack += 1
        elements.append(element)
    }

    func pop() -> Int? {
        if topOfStack == 0 {
            return nil
        } else {
            let element = elements[topOfStack]
            elements.removeLast()
            topOfStack -= 1
            return element
        }
    }
}
```

## Maintaining Cohesion Results in Many Small Classes

Just the act of breaking large functions into smaller functions causes a proliferation of classes.

# Swift Specific

## Delegates

When creating custom delegate methods, an unnamed first parameter should be the delegate source. (UIKit contains numerous examples of this.)

{- Not Preferred -}
```swift
func didSelectName(namePicker: NamePickerViewController, name: String)
func namePickerShouldReload() -> Bool
```

{+ Preferred +}
```swift
func namePickerView(_ namePickerView: NamePickerView, didSelectName name: String)
func namePickerViewShouldReload(_ namePickerView: NamePickerView) -> Bool
```

## Use Type Inferred Context

Use compiler inferred context to write shorter, clear code.

{- Not Preferred -}
```swift
let selector = #selector(ViewController.viewDidLoad)
view.backgroundColor = UIColor.red
let toView = context.view(forKey: UITransitionContextViewKey.to)
let view = UIView(frame: CGRect.zero)
```

{+ Preferred +}
```swift
let selector = #selector(viewDidLoad)
view.backgroundColor = .red
let toView = context.view(forKey: .to)
let view = UIView(frame: .zero)
```

## Unused Code

Unused (dead) code, including Xcode template code and placeholder comments should be removed. An exception is when your tutorial or book instructs the user to use the commented code.

{- Not Preferred -}
```swift
override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}

override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
}

override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return Database.contacts.count
}
```

{+ Preferred +}
```swift
override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Database.contacts.count
}
```

## Use of Self

For conciseness, avoid using self since Swift does not require it to access an object's properties or invoke its methods.

Use self only when required by the compiler (in @escaping closures, or in initializers to disambiguate properties from arguments). In other words, if it compiles without self then omit it.

## Constants

Constants are defined using the let keyword, and variables with the var keyword. Always use let instead of var if the value of the variable will not change.

Tip: A good technique is to define everything using let and only change it to var if the compiler complains!

You can define constants on a type rather than on an instance of that type using type properties. To declare a type property as a constant simply use static let. Type properties declared in this way are generally preferred over global constants because they are easier to distinguish from instance properties. Example:

{- Not Preferred -}
```swift
let e = 2.718281828459045235360287  // pollutes global namespace
let root2 = 1.41421356237309504880168872

let hypotenuse = side * root2 // what is root2?
```

{+ Preferred +}
```swift
enum Math {
    static let e = 2.718281828459045235360287
    static let root2 = 1.41421356237309504880168872
}

let hypotenuse = side * Math.root2
```

## Optionals

Declare variables and function return types as optional with ? where a nil value is acceptable.

Use implicitly unwrapped types declared with ! only for instance variables that you know will be initialized later before use, such as subviews that will be set up in viewDidLoad.

When accessing an optional value, use optional chaining if the value is only accessed once or if there are many optionals in the chain:

```swift
self.textContainer?.textLabel?.setNeedsDisplay()
```

Use optional binding when it's more convenient to unwrap once and perform multiple operations:

```swift
if let textContainer = self.textContainer {
    // do many things with textContainer
}
```

When naming optional variables and properties, avoid naming them like optionalString or maybeView since their optional-ness is already in the type declaration.

For optional binding, shadow the original name when appropriate rather than using names like unwrappedView or actualLabel.

{- Not Preferred -}
```swift
var optionalSubview: UIView?
var volume: Double?

if let unwrappedSubview = optionalSubview {
    if let realVolume = volume {
        // do something with unwrappedSubview and realVolume
    }
}
```

{+ Preferred +}
```swift
var subview: UIView?
var volume: Double?

// later on...
if let subview = subview, let volume = volume {
    // do something with unwrapped subview and volume
}
```

## Type Inference

Prefer compact code and let the compiler infer the type for constants or variables of single instances. Type inference is also appropriate for small (non-empty) arrays and dictionaries. When required, specify the specific type such as CGFloat or Int16.

{- Not Preferred -}
```swift
let message: String = "Click the button"
let currentBounds: CGRect = computeViewBounds()
let names = [String]()
```

{+ Preferred +}
```swift
let message = "Click the button"
let currentBounds = computeViewBounds()
var names = ["Mic", "Sam", "Christine"]
let maximumWidth: CGFloat = 106.5
```

## Type Annotation for Empty Arrays and Dictionaries

For empty arrays and dictionaries, use type annotation. (For an array or dictionary assigned to a large, multi-line literal, use type annotation.)

{- Not Preferred -}
```swift
var names = [String]()
var lookup = [String: Int]()
```

{+ Preferred +}
```swift
var names: [String] = []
var lookup: [String: Int] = [:]
```

## Memory Management

Code (even non-production, tutorial demo code) should not create reference cycles. Analyze your object graph and prevent strong cycles with weak and unowned references. Alternatively, use value types (struct, enum) to prevent cycles altogether.

Extend object lifetime using the [weak self] and guard let strongSelf = self else { return } idiom. [weak self] is preferred to [unowned self] where it is not immediately obvious that self outlives the closure. Explicitly extending lifetime is preferred to optional unwrapping.

{- Not Preferred -}
```swift
// might crash if self is released before response returns
resource.request().onComplete { [unowned self] response in
    let model = self.updateModel(response)
    self.updateUI(model)
}
```

{+ Preferred +}
```swift
resource.request().onComplete { [weak self] response in
    guard let strongSelf = self else {
        return
    }
    let model = strongSelf.updateModel(response)
    strongSelf.updateUI(model)
}
```

## Access Control

Full access control annotation in tutorials can distract from the main topic and is not required. Using private and fileprivate appropriately, however, adds clarity and promotes encapsulation. Prefer private to fileprivate when possible. Using extensions may require you to use fileprivate.

Only explicitly use open, public, and internal when you require a full access control specification.

Use access control as the leading property specifier. The only things that should come before access control are the static specifier or attributes such as @IBAction, @IBOutlet and @discardableResult.

{- Not Preferred -}
```swift
fileprivate let message = "Great Scott!"

class TimeMachine {
    lazy dynamic fileprivate var fluxCapacitor = FluxCapacitor()
}
```

{+ Preferred +}
```swift
private let message = "Great Scott!"

class TimeMachine {
    fileprivate dynamic lazy var fluxCapacitor = FluxCapacitor()
}
```
## Golden Path

When coding with conditionals, the left-hand margin of the code should be the "golden" or "happy" path. That is, don't nest if statements. Multiple return statements are OK. The guard statement is built for this.

{- Not Preferred -}
```swift
func computeFFT(context: Context?, inputData: InputData?) throws -> Frequencies {

    if let context = context {
        if let inputData = inputData {
            // use context and input to compute the frequencies
            return frequencies
        } else {
            throw FFTError.noInputData
        }
    } else {
        throw FFTError.noContext
    }
}
```

{+ Preferred +}
```swift
func computeFFT(context: Context?, inputData: InputData?) throws -> Frequencies {

    guard let context = context else {
        throw FFTError.noContext
    }
    guard let inputData = inputData else {
        throw FFTError.noInputData
    }

    // use context and input to compute the frequencies
    return frequencies
}
```
When multiple optionals are unwrapped either with guard or if let, minimize nesting by using the compound version when possible. Example:

{- Not Preferred -}
```swift
if let number1 = number1 {
    if let number2 = number2 {
        if let number3 = number3 {
            // do something with numbers
        } else {
            fatalError("impossible")
        }
    } else {
        fatalError("impossible")
    }
} else {
    fatalError("impossible")
}
```

{+ Preferred +}
```swift
guard let number1 = number1,
      let number2 = number2,
      let number3 = number3 else {
      fatalError("impossible")
}
// do something with numbers
```

